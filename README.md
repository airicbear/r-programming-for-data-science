# R Programming for Data Science

Roger D. Peng

## Objective

Read this book for independent reading assignment.

## Motivation

I chose this book because it looks easy to read and I'm interested in learning R.

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Subsetting R Objects"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are three operators that can be used to extract subsets of R objects.\n",
    "\n",
    "- The `[` operator always returns an object of the same class as the original. It can be used to select multiple elements of an object.\n",
    "- The `[[` operator is used to extract elements of a list or a data frame. It can only be used to extract a single element and the class of the returned object will not necessarily be a list or data frame.\n",
    "- The `$` operator is used to extract elements of a list or data frame by literal name. Its semantics are similar to that of `[[`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Subsetting a Vector"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vectors are basic objects in R and they can be subsetted using the `[` operator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"a\"\n"
     ]
    }
   ],
   "source": [
    "x <- c(\"a\", \"b\", \"c\", \"c\", \"d\", \"a\")\n",
    "print(x[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"b\"\n"
     ]
    }
   ],
   "source": [
    "print(x[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `[` operator can be used to extract multiple elements of a vector by passing the operator an integer sequence. Here we extract the first four elements of the vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"a\" \"b\" \"c\" \"c\"\n"
     ]
    }
   ],
   "source": [
    "print(x[1:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The sequence does not have to be in order; you can specify any arbitrary integer vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"a\" \"c\" \"c\"\n"
     ]
    }
   ],
   "source": [
    "print(x[c(1, 3, 4)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also pass a logical sequence to the `[` operator to extract elements of a vector that satisfy a given condition. For example, here we want the elements of `x` that come lexicographically *after* the letter \"a\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE  TRUE  TRUE  TRUE  TRUE FALSE\n"
     ]
    }
   ],
   "source": [
    "u <- x > \"a\"\n",
    "print(u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"b\" \"c\" \"c\" \"d\"\n"
     ]
    }
   ],
   "source": [
    "print(x[u])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another, more compact, way to do this would be to skip the creation of a logical vector and just subset the vector directly with the logical expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"b\" \"c\" \"c\" \"d\"\n"
     ]
    }
   ],
   "source": [
    "print(x[x > \"a\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Subsetting a Matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrices can be subsetted in the usual way with $(i,j)$ type indices. Here, we create simple $2\\times3$ matrix with the `matrix` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     [,1] [,2] [,3]\n",
      "[1,]    1    3    5\n",
      "[2,]    2    4    6\n"
     ]
    }
   ],
   "source": [
    "x <- matrix(1:6, 2, 3)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access the $(1, 2)$ or the $(2, 1)$ element of this matrix using the appropriate indices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 3\n"
     ]
    }
   ],
   "source": [
    "print(x[1, 2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 2\n"
     ]
    }
   ],
   "source": [
    "print(x[2, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indices can also be missing. This behavior is used to access entire rows or columns of a matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 3 5\n"
     ]
    }
   ],
   "source": [
    "print(x[1,])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 3 4\n"
     ]
    }
   ],
   "source": [
    "print(x[,2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dropping matrix dimensions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, when a single element of a matrix is retrieved, it is returned as a vector of length $1$ rather than a $1 \\times 1$ matrix. Often, this is exactly what we want, but this behavior can be turned off by setting `drop = FALSE`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 3\n"
     ]
    }
   ],
   "source": [
    "x <- matrix(1:6, 2, 3)\n",
    "print(x[1, 2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     [,1]\n",
      "[1,]    3\n"
     ]
    }
   ],
   "source": [
    "print(x[1, 2, drop = FALSE])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, when we extract a single row or column of a matrix, R by default drops the dimension of length $1$, so instead, so instead of getting a $1 \\times 3$ matrix after extracting the first row, we get a vector of length $3$. This behavior can similarly be turned off with the `drop = FALSE` option."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 3 5\n"
     ]
    }
   ],
   "source": [
    "x <- matrix(1:6, 2, 3)\n",
    "print(x[1,])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     [,1] [,2] [,3]\n",
      "[1,]    1    3    5\n"
     ]
    }
   ],
   "source": [
    "print(x[1, , drop = FALSE])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Be careful of R's automatic dropping dimensions.** This is a feature that is quite often useful during interactive work, but can later come back to bite you when you are writing longer programs or functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Subsetting Lists"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists in R can be subsetted using all three of the operators mentions above, and all three are used for different purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "$foo\n",
      "[1] 1 2 3 4\n",
      "\n",
      "$bar\n",
      "[1] 0.6\n",
      "\n"
     ]
    }
   ],
   "source": [
    "x <- list(foo = 1:4, bar = 0.6)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `[[` operator can be used to extract *single* elements from a list. Here we extract the file element of the list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 3 4\n"
     ]
    }
   ],
   "source": [
    "print(x[[1]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 0.6\n"
     ]
    }
   ],
   "source": [
    "print(x[[\"bar\"]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NULL\n"
     ]
    }
   ],
   "source": [
    "print(x$Bar)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice you don't need the quotes when you use the `$` operator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One thing that differentiates the `[[` operator from the `$` is that the `[[` operator can be used with *computed* indices. The `$` operator can only be used with literal names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- list(foo = 1:4, bar = 0.6, baz = \"hello\")\n",
    "name <- \"foo\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 3 4\n"
     ]
    }
   ],
   "source": [
    "## computed index for \"foo\"\n",
    "print(x[[name]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NULL\n"
     ]
    }
   ],
   "source": [
    "## element \"name\" doesn't exist! (but no error here)\n",
    "print(x$name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 3 4\n"
     ]
    }
   ],
   "source": [
    "## element \"foo\" does exist\n",
    "print(x$foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Subsetting Nested Elements of a List"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `[[` operator can take an integer sequence if you want to extract a nested element of a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- list(a = list(10, 12, 14), b = c(3.14, 2.81))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 3\n"
     ]
    }
   ],
   "source": [
    "## Get the 3rd element of the 1st element\n",
    "print(x[[c(1, 3)]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 14\n"
     ]
    }
   ],
   "source": [
    "## Same as above\n",
    "print(x[[1]][[3]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 3.14\n"
     ]
    }
   ],
   "source": [
    "## 1st element of the 2nd element\n",
    "print(x[[c(2, 1)]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extracting Multiple Elements of a List"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `[` operator can be used to extract *multiple* elements from a list. For example, if you wanted to extract the first and third elements of a list, you would do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "$foo\n",
      "[1] 1 2 3 4\n",
      "\n",
      "$baz\n",
      "[1] \"hello\"\n",
      "\n"
     ]
    }
   ],
   "source": [
    "x <- list(foo = 1:4, bar = 0.6, baz = \"hello\")\n",
    "print(x[c(1, 3)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that `x[c(1, 3)]` is NOT the same as `x[[c(1, 3)]]`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that the `[` operator always returns an object of the same class as the original. Since the original object was a list, the `[` operator returns a list. In the above code, we returned a list with two elements (the first and third)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Partial Matching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Partial matching of names is allowed with `[[` and `$`. This is often very useful during interactive work if the object you're working with has very long element names. You can just abbreviate those names and R will figure out what element you're referring to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 3 4 5\n"
     ]
    }
   ],
   "source": [
    "x <- list(aardvark = 1:5)\n",
    "print(x$a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NULL\n"
     ]
    }
   ],
   "source": [
    "print(x[[\"a\"]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 3 4 5\n"
     ]
    }
   ],
   "source": [
    "print(x[[\"a\", exact = FALSE]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, this is fine for interactive work, but you shouldn't resort to partial matching if you are writing longer scripts, functions, or programs. In those cases, you should refer to the full element name if possible. That way there's no ambiguity in your code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Removing NA Values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A common task in data analysis is removing missing values (`NA`s)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE FALSE  TRUE FALSE  TRUE FALSE\n"
     ]
    }
   ],
   "source": [
    "x <- c(1, 2, NA, 4, NA, 5)\n",
    "bad <- is.na(x)\n",
    "print(bad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 4 5\n"
     ]
    }
   ],
   "source": [
    "print(x[!bad])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if there are multiple R objects and you want to take the subset with no missing values in any of those objects?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1]  TRUE  TRUE FALSE  TRUE FALSE  TRUE\n"
     ]
    }
   ],
   "source": [
    "x <- c(1, 2, NA, 4, NA, 5)\n",
    "y <- c(\"a\", \"b\", NA, \"d\", NA, \"f\")\n",
    "good <- complete.cases(x, y)\n",
    "print(good)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1 2 4 5\n"
     ]
    }
   ],
   "source": [
    "print(x[good])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"a\" \"b\" \"d\" \"f\"\n"
     ]
    }
   ],
   "source": [
    "print(y[good])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use `complete.cases` on data frames too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  Ozone Solar.R Wind Temp Month Day\n",
      "1    41     190  7.4   67     5   1\n",
      "2    36     118  8.0   72     5   2\n",
      "3    12     149 12.6   74     5   3\n",
      "4    18     313 11.5   62     5   4\n",
      "5    NA      NA 14.3   56     5   5\n",
      "6    28      NA 14.9   66     5   6\n"
     ]
    }
   ],
   "source": [
    "print(head(airquality))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [],
   "source": [
    "good <- complete.cases(airquality)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  Ozone Solar.R Wind Temp Month Day\n",
      "1    41     190  7.4   67     5   1\n",
      "2    36     118  8.0   72     5   2\n",
      "3    12     149 12.6   74     5   3\n",
      "4    18     313 11.5   62     5   4\n",
      "7    23     299  8.6   65     5   7\n",
      "8    19      99 13.8   59     5   8\n"
     ]
    }
   ],
   "source": [
    "print(head(airquality[good,]))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

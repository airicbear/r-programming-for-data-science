{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# R Nuts and Bolts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Entering Input\n",
    "\n",
    "At the R prompt we type expressions. The `<-` symbol is the assignment operator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 1\n"
     ]
    }
   ],
   "source": [
    "x <- 1\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "1"
      ],
      "text/latex": [
       "1"
      ],
      "text/markdown": [
       "1"
      ],
      "text/plain": [
       "[1] 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "msg <- \"hello\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The grammar of the language determines whether an expression is complete or not."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "ename": "ERROR",
     "evalue": "Error in parse(text = x, srcfile = src): <text>:2:0: unexpected end of input\n1: x <- ## Incomplete expression\n   ^\n",
     "output_type": "error",
     "traceback": [
      "Error in parse(text = x, srcfile = src): <text>:2:0: unexpected end of input\n1: x <- ## Incomplete expression\n   ^\nTraceback:\n"
     ]
    }
   ],
   "source": [
    "x <- ## Incomplete expression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `#` character indicates a comment. Anything to the right side of the `#` (including the `#` itself) is ignored. This is the only comment character in R. Unlike some other languages, R does not support multi-line comments or comment blocks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation\n",
    "\n",
    "When a complete expression is entered at the prompt, it is evaluated and the result of the evaluated expression is returned. The result may be *auto-printed*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- 5 ## nothing printed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "5"
      ],
      "text/latex": [
       "5"
      ],
      "text/markdown": [
       "5"
      ],
      "text/plain": [
       "[1] 5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x ## auto-printing occurs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 5\n"
     ]
    }
   ],
   "source": [
    "print(x) ## explicit printing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `[1]` shown in the output indicates that `x` is a vector and `5` is its first element."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Typically with interactive work, we do not explicity print objects with the `print` function; it is much easier to just auto-print them by typing the name of the object and hitting return/enter. However, when writing scripts, functions, or longer programs, there is sometimes a need to explicitly print objects because auto-printing does not work in those settings."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When an R vector is printed you may see square brackets `[]` on the side. For example, see this integer sequence of length `50`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- 10:59"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " [1] 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34\n",
      "[26] 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The numbers in the square brackets are not part of the vector itself, they are merely part of the *printed output*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With R, it's important that one understands that there is a difference between the actual R object and the manner in which that R object is printed to the console. Often, the printed output may have additional bells and whistles to make the output more friendly to the users. However, these bells and whistles are not inherently part of the object."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the `:` operator is used to create integer sequences."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## R Objects\n",
    "\n",
    "R has five basic or \"atomic\" classes of objects:\n",
    "\n",
    "- character\n",
    "- numeric (real numbers)\n",
    "- integer\n",
    "- complex\n",
    "- logical (True/False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most basic type of R object is a vector. Empty vectors can be created with the `vector()` function. There is really only one rule about vectors in R, which is that **A vector can only contain objects of the same class.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But of course, like any good rule, there is an exception&mdash;*lists*. Lists are represented as vectors, but they can contain objects of different classes. Indeed, that's usually why we use them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a class for \"raw\" objects, but they are not commonly used directly in data analysis and will not be covered. For more information, [check out the documentation](https://stat.ethz.ch/R-manual/R-devel/library/base/html/raw.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numbers\n",
    "\n",
    "Numbers in R are generally treated as numeric objects (i.e double precision real numbers). This means that even if you see a number like \"1\" or \"2\" in R, which you might think of as integers, they are likely represented behind the scenes as numeric objects (so something like \"1.00\" or \"2.00\"). This isn't important most of the time...except when it is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you explicitly want an integer, you need to specify the `L` suffix. So entering `1` in R gives you a numeric object; entering `1L` explicitly gives you an integer object."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a special number `Inf` which represents infinity. This allows us to represent entities like `1 / 0`. This way, `Inf` can be used in ordinary calculations; e.g. `1 / Inf` is `0`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value `NaN` represents an undefined value (\"not a number\"); e.g. `0 / 0; NaN` can also be though of as a missing value (more on that later)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attributes\n",
    "\n",
    "R objects can have attributes, which are like metadata for the object. These metadata can be very useful in that they help to describe the object. For example, column names on a data frame help to tell us what data are containede in each of the columns. Some examples of R object attributes are\n",
    "\n",
    "- names, dimnames\n",
    "- dimensions (e.g. matrices, arrays)\n",
    "- class (e.g. integer, numeric)\n",
    "- length\n",
    "- other user-defined attributes/metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Attributes of an object (if any) can be accessed using the `attributes()` function. Not all R objects contain attributes, in which case the `attributes()` function returns `NULL`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating Vectors\n",
    "\n",
    "The `c()` function can be used to create vectors of objects by concatenating things together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'numeric'"
      ],
      "text/latex": [
       "'numeric'"
      ],
      "text/markdown": [
       "'numeric'"
      ],
      "text/plain": [
       "[1] \"numeric\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- c(0.5, 0.6)\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'logical'"
      ],
      "text/latex": [
       "'logical'"
      ],
      "text/markdown": [
       "'logical'"
      ],
      "text/plain": [
       "[1] \"logical\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- c(TRUE, FALSE)\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'logical'"
      ],
      "text/latex": [
       "'logical'"
      ],
      "text/markdown": [
       "'logical'"
      ],
      "text/plain": [
       "[1] \"logical\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- c(T, F)\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'character'"
      ],
      "text/latex": [
       "'character'"
      ],
      "text/markdown": [
       "'character'"
      ],
      "text/plain": [
       "[1] \"character\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- c(\"a\", \"b\", \"c\")\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'integer'"
      ],
      "text/latex": [
       "'integer'"
      ],
      "text/markdown": [
       "'integer'"
      ],
      "text/plain": [
       "[1] \"integer\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- 9:29\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'complex'"
      ],
      "text/latex": [
       "'complex'"
      ],
      "text/markdown": [
       "'complex'"
      ],
      "text/plain": [
       "[1] \"complex\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- c(1+0i, 2+4i)\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in the above example, `T` and `F` are short-hand ways to specify `TRUE` and `FALSE`. However, in general one should try to use the explicit `TRUE` and `FALSE` values when indicating logical values. The `T` and `F` values are primarily there for when you're feeling lazy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use the `vector()` function to initialize vectors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " [1] 0 0 0 0 0 0 0 0 0 0\n"
     ]
    }
   ],
   "source": [
    "x <- vector(\"numeric\", length = 10)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mixing Objects\n",
    "\n",
    "There are occasions when different classes of R objects get mixed together. Sometimes this happens by accident but it can also happen on purpose. So what happens with the following code?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'character'"
      ],
      "text/latex": [
       "'character'"
      ],
      "text/markdown": [
       "'character'"
      ],
      "text/plain": [
       "[1] \"character\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "y <- c(1.7, \"a\")\n",
    "class(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'numeric'"
      ],
      "text/latex": [
       "'numeric'"
      ],
      "text/markdown": [
       "'numeric'"
      ],
      "text/plain": [
       "[1] \"numeric\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "y <- c(TRUE, 2)\n",
    "class(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'character'"
      ],
      "text/latex": [
       "'character'"
      ],
      "text/markdown": [
       "'character'"
      ],
      "text/plain": [
       "[1] \"character\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "y <- c(\"a\", TRUE)\n",
    "class(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In each case above, we are mixing objects of two different classes in a vector. But remember that the only rule about vectors says this is not allowed. When different objects are mixed in a vector, *coercion* occurs so that every element in the vector is of the same class."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, we see the effect of *implicit coercion*. What R tries to do is find a way to represent all of the objects in the vector in a reasonable fashion. Sometimes this does exactly what you want and...sometimes not. For example, combining a numeric object with a character object will create a character vector, because numbers can usually be easily represented as strings."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Explicit Coercion\n",
    "\n",
    "Objects can be explicitly coerced from one class to another using the `as.*` functions, if available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "'integer'"
      ],
      "text/latex": [
       "'integer'"
      ],
      "text/markdown": [
       "'integer'"
      ],
      "text/plain": [
       "[1] \"integer\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x <- 0:6\n",
    "class(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 0 1 2 3 4 5 6\n"
     ]
    }
   ],
   "source": [
    "print(as.numeric(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE\n"
     ]
    }
   ],
   "source": [
    "print(as.logical(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"0\" \"1\" \"2\" \"3\" \"4\" \"5\" \"6\"\n"
     ]
    }
   ],
   "source": [
    "print(as.character(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, R can't figure out how to coerce an object and this can result in `NA`s being produced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message in print(as.numeric(x)):\n",
      "\"NAs introduced by coercion\""
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] NA NA NA\n"
     ]
    }
   ],
   "source": [
    "x <- c(\"a\", \"b\", \"c\")\n",
    "print(as.numeric(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] NA NA NA\n"
     ]
    }
   ],
   "source": [
    "print(as.logical(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message in print(as.complex(x)):\n",
      "\"NAs introduced by coercion\""
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] NA NA NA\n"
     ]
    }
   ],
   "source": [
    "print(as.complex(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When nonsensical coercion takes place, you will usually get a warning from R."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matrices\n",
    "\n",
    "Matrices are vectors with a *dimension* attribute. The dimension attribute is itself an integer vector of length 2 (number of rows, number of columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     [,1] [,2] [,3]\n",
      "[1,]   NA   NA   NA\n",
      "[2,]   NA   NA   NA\n"
     ]
    }
   ],
   "source": [
    "m <- matrix(nrow = 2, ncol = 3)\n",
    "print(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 2 3\n"
     ]
    }
   ],
   "source": [
    "print(dim(m))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "$dim\n",
      "[1] 2 3\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(attributes(m))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrices are constructed *column-wise*, so entries can be thought of starting in the \"upper left\" corner and running down the columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     [,1] [,2] [,3]\n",
      "[1,]    1    3    5\n",
      "[2,]    2    4    6\n"
     ]
    }
   ],
   "source": [
    "m <- matrix(1:6, nrow = 2, ncol = 3)\n",
    "print(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrices can also be created directly from vectors by adding a dimension attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " [1]  1  2  3  4  5  6  7  8  9 10\n"
     ]
    }
   ],
   "source": [
    "m <- 1:10\n",
    "print(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     [,1] [,2] [,3] [,4] [,5]\n",
      "[1,]    1    3    5    7    9\n",
      "[2,]    2    4    6    8   10\n"
     ]
    }
   ],
   "source": [
    "dim(m) <- c(2, 5)\n",
    "print(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrices can be created by *column-binding* or *row-binding* with the `cbind()` and `rbind()` functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- 1:3\n",
    "y <- 10:12"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     x  y\n",
      "[1,] 1 10\n",
      "[2,] 2 11\n",
      "[3,] 3 12\n"
     ]
    }
   ],
   "source": [
    "print(cbind(x, y))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  [,1] [,2] [,3]\n",
      "x    1    2    3\n",
      "y   10   11   12\n"
     ]
    }
   ],
   "source": [
    "print(rbind(x, y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lists\n",
    "\n",
    "Lists are a special type of vector that can contain elements of different classes. Lists are a very important data type in R and you should get to know them well. Lists, in combination with various \"apply\" functions discussed later, make for a powerful combination."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists can be explicitly created using the `list()` function, which takes an arbitrary number of arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- list(1, \"a\", TRUE, 1 + 4i)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1]]\n",
      "[1] 1\n",
      "\n",
      "[[2]]\n",
      "[1] \"a\"\n",
      "\n",
      "[[3]]\n",
      "[1] TRUE\n",
      "\n",
      "[[4]]\n",
      "[1] 1+4i\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also create an empty list of a prespecified length with the `vector()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- vector(\"list\", length = 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1]]\n",
      "NULL\n",
      "\n",
      "[[2]]\n",
      "NULL\n",
      "\n",
      "[[3]]\n",
      "NULL\n",
      "\n",
      "[[4]]\n",
      "NULL\n",
      "\n",
      "[[5]]\n",
      "NULL\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Factors\n",
    "\n",
    "Factors are used to represent categorical data and can be unordered or ordered. One can think of a factor as an integer vector where each integer has a *label*. Factors are important in statistical modeling and are treated specially by modelling functions like `lm()` and `glm()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using factors with labels is *better* than using integers because factors are self-describing. Having a variable that has values \"Male\" and \"Female\" is better than a variable that has values 1 and 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Factor objects can be created with the `factor()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- factor(c(\"yes\", \"yes\", \"no\", \"yes\", \"no\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] yes yes no  yes no \n",
      "Levels: no yes\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x\n",
      " no yes \n",
      "  2   3 \n"
     ]
    }
   ],
   "source": [
    "print(table(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 2 2 1 2 1\n",
      "attr(,\"levels\")\n",
      "[1] \"no\"  \"yes\"\n"
     ]
    }
   ],
   "source": [
    "## See the underlying representation of factor\n",
    "print(unclass(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often factors will be automatically created for you when you read a dataset in using a function like `read.table()`. Those functions often default to creating factors when they encounter data that look like characters or strings."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The order of the levels of a factor can be set usingithe `levels` argument to `factor()`. This can be important in linear modelling because the first level is used as the baseline level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- factor(c(\"yes\", \"yes\", \"no\", \"yes\", \"no\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] yes yes no  yes no \n",
      "Levels: no yes\n"
     ]
    }
   ],
   "source": [
    "print(x) ## Levels are put in alphabetical order"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- factor(c(\"yes\", \"yes\", \"no\", \"yes\", \"no\"),\n",
    "            levels = c(\"yes\", \"no\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] yes yes no  yes no \n",
      "Levels: yes no\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Missing Values\n",
    "\n",
    "Missing values are denoted by `NA` or `NaN` for q undefined mathematical operations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `is.na()` is used to test objects if they are `NA`\n",
    "- `is.nan()` is used to test for `NaN`\n",
    "- `NA` values have a class also, so there are integer `NA`, character `NA`, etc.\n",
    "- A `NaN` value is also `NA` but the converse is not true"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Create a vector with NAs in it\n",
    "x <- c(1, 2, NA, 10, 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE FALSE  TRUE FALSE FALSE\n"
     ]
    }
   ],
   "source": [
    "print(is.na(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE FALSE FALSE FALSE FALSE\n"
     ]
    }
   ],
   "source": [
    "print(is.nan(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Now create a vector with both NA and NaN values\n",
    "x <- c(1, 2, NaN, NA, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE FALSE  TRUE  TRUE FALSE\n"
     ]
    }
   ],
   "source": [
    "print(is.na(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] FALSE FALSE  TRUE FALSE FALSE\n"
     ]
    }
   ],
   "source": [
    "print(is.nan(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Frames\n",
    "\n",
    "Data frames are used to store tabular data in R. They are an important type of object in R and are used in a variety of statistical modeling applications. Hadley Wickham's package, \"dplyr\", has an optimized set of functions designed to work efficiently with data frames."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data frames are represented as a special type of list where every element of the list has to have the same length. Each element of the list can be thought of as a column and the length of each element of the list is the number of rows."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike matrices, data frames can store different classes of objects in each column. Matrices must have every element be the same class (e.g. all integers or all numeric)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to column names, indicating the names of the variables or predictors, data frames have a special attribute callede `row.names` which indicate information about each row of the data frame."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data frames are usually created by reading in a dataset using the `read.table()` or `read.csv()`. However, data frames can also be created explicitly with the `data.frame()` function or they can be coerced from other types of objects like lists."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data frames can be converted to a matrix by calling `data.matrix()`. While it might seem that the `as.matrix()` function should be used to coerce a data frame to a matrix, almost always, what you want is the result of `data.matrix()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- data.frame(foo = 1:4, bar = c(T, T, F, F))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  foo   bar\n",
      "1   1  TRUE\n",
      "2   2  TRUE\n",
      "3   3 FALSE\n",
      "4   4 FALSE\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 4\n"
     ]
    }
   ],
   "source": [
    "print(nrow(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] 2\n"
     ]
    }
   ],
   "source": [
    "print(ncol(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Names\n",
    "\n",
    "R objects can have names, which is very useful for writing readable code and self-describing objects. Here is an example of assigning names to an integer vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- 1:3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NULL\n"
     ]
    }
   ],
   "source": [
    "print(names(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    "names(x) <- c(\"New York\", \"Seattle\", \"Lost Angeles\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    New York      Seattle Lost Angeles \n",
      "           1            2            3 \n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"New York\"     \"Seattle\"      \"Lost Angeles\"\n"
     ]
    }
   ],
   "source": [
    "print(names(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists can also have names, which is often very useful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- list(\"Los Angeles\" = 1, Boston = 2, London = 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "$`Los Angeles`\n",
      "[1] 1\n",
      "\n",
      "$Boston\n",
      "[1] 2\n",
      "\n",
      "$London\n",
      "[1] 3\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Los Angeles\" \"Boston\"      \"London\"     \n"
     ]
    }
   ],
   "source": [
    "print(names(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrices can have both column and row names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [],
   "source": [
    "m <- matrix(1:4, nrow = 2, ncol = 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [],
   "source": [
    "dimnames(m) <- list(c(\"a\", \"b\"), c(\"c\", \"d\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  c d\n",
      "a 1 3\n",
      "b 2 4\n"
     ]
    }
   ],
   "source": [
    "print(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Column names and row names can be set separately using the `colnames()` and `rownames()` functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [],
   "source": [
    "colnames(m) <- c(\"h\", \"f\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [],
   "source": [
    "rownames(m) <- c(\"x\", \"z\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  h f\n",
      "x 1 3\n",
      "z 2 4\n"
     ]
    }
   ],
   "source": [
    "print(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that for data frames, there is a separate function for setting the row names, the `row.names()` function. Also, data frames do not have column names, they just have names (like lists). So to set the column names of a data frame just use the `names()` function. Yes, I know its confusing. Here's a quick summary:\n",
    "\n",
    "| Object | Set column names | Set row names |\n",
    "| --- | --- | --- |\n",
    "| data frame | `names()` | `row.names()` |\n",
    "| matrix | `colnames()` | `rownames()` |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary\n",
    "\n",
    "There are a variety of different builtin-data types in R. In this chapter we have reviewed the following:\n",
    "\n",
    "- atomic classes: numeric, logical, character, integer, complex\n",
    "- vectors, lists\n",
    "- factors\n",
    "- missing values\n",
    "- data frames and matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All R objects can have attributes that help to describe what is in the object. Perhaps the most useful attribute is names, such as column and row names in a data frame, or simply names in a vector or list. Attributes like dimensions are also important as they can modify the behavior of objects, like turning a vector into a matrix."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
